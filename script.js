/*
Чому для роботи з input не рекомендується використовувати клавіатуру?
 Якщо ми хочемо коректно відслідковувати ввод в поле input, то використання лише  клавіатури буде недостатнім.

 На відміну від подій клавіатури, input запускається при будь-якій зміні значень, навіть тих, які не передбачають дії
 клавіатури: вставлення тексту за допомогою миші або використання розпізнавання мовлення для диктування тексту.

 Подія input не запускається під час введення з клавіатури та інших дій, які не передбачають зміну значення.
 */

const buttons = document.querySelectorAll(".btn");
document.documentElement.addEventListener("keydown", (event) => {
    buttons.forEach(button => {
        let buttonCode = "";
        if (button.innerText == "Enter") {
            buttonCode = "Enter";
        } else {
            buttonCode = "Key" + button.innerText;
        };
        button.style.backgroundColor = "";
        if (buttonCode == event.code) {
            button.style.backgroundColor = "blue";
        };
    });
})

